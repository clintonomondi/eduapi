<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawalLogs extends Model
{
    use HasFactory;

    protected  $fillable=['amount','amount_to_pay','charges','TransactionID','balance','phone','system_ref','ConversationID','OriginatorConversationID','ResponseCode','ResponseDescription','status','date','user','user_id','withdraw_charges','account_balance'];
}
