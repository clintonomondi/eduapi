<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected  $fillable=['user_id','category_id','subject_id','title','description','amount','url','level','darasa_id'];
}
