<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected  $fillable=['order_no','user_id','created_by','status','payment_method','trans_id','system_id'];
}
