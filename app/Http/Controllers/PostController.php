<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public  function post(Request $request){
        $request['user_id']=Auth::user()->id;
        $post=Post::create($request->all());
        return ['status'=>true,'message'=>'Post created successfully'];
    }

    public  function getPosts(){
        $datas = DB::select( DB::raw("SELECT *,
(SELECT name FROM users B WHERE B.id=A.user_id )auther,
(SELECT name FROM categories B WHERE B.id=A.category_id )category
 FROM posts A order   By id desc") );
        return ['posts'=>$datas];
    }
}
