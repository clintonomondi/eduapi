<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Darasa;
use App\Models\Subject;
use Illuminate\Http\Request;


class DarasaController extends Controller
{
    public  function post(Request $request){
        $data=Darasa::create($request->all());
        return ['status'=>true,'message'=>'Data submitted successfully'];
    }

    public  function index($id){
        $category=Category::find($id);
        $data=Darasa::where('category_id',$id)->get();
        return ['category'=>$category,'darasa'=>$data];
    }
    public  function getCategoryDarasa(Request $request){
        $darasa=Darasa::where('category_id',$request->category_id)->get();
        return ['darasa'=>$darasa];
    }
    public function updateDarasa(Request $request){
        $da=Darasa::find($request->id);
        $da->update($request->all());
        return ['status'=>true,'message'=>'Data updated successfully'];
    }
}
