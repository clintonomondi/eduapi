<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderRecords;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public  function placeOrder(Request $request){
      $request['order_no']='ED'.mt_rand(10000,99999).Auth::user()->id;
      $request['created_by']=Auth::user()->id;
      $request['user_id']=Auth::user()->id;
      $order=Order::create($request->all());

      foreach ($request->data as $records){
       $request['order_id']=$order->id;
       $request['amount']=$records['amount'];
       $request['book_id']=$records['id'];
       $data=OrderRecords::create($request->all());
      }

      return ['status'=>true,'message'=>'Order placed successfully, proceed to payment','id'=>$order->id];
    }

    public  function getOrder($id){
        $user=User::find(Auth::user()->id);
        $total=OrderRecords::where('order_id',$id)->sum('amount');
        $order=Order::find($id);

        $records = DB::select( DB::raw("SELECT *,
(SELECT title FROM books B WHERE B.id=A.book_id)title
 FROM order_records A WHERE order_id='$id'") );


        return ['user'=>$user,'total'=>$total,'order'=>$order,'records'=>$records];
    }

    public  function orders(){
        $user_id=Auth::user()->id;
        $orders = DB::select( DB::raw("SELECT *,
(SELECT count(*) FROM order_records B WHERE B.order_id=A.id)items,
(SELECT sum(amount) FROM order_records B WHERE B.order_id=A.id)total
 FROM orders A WHERE user_id='$user_id'") );


        return ['orders'=>$orders];
    }


    public  function downloads(){
        $user_id=Auth::user()->id;
        $orders = DB::select( DB::raw("SELECT *,
(SELECT order_no FROM orders B WHERE B.id=A.order_id)order_no,
(SELECT title FROM books B WHERE B.id=A.book_id)title,
(SELECT url FROM books B WHERE B.id=A.book_id)url
 FROM `order_records` A WHERE STATUS='PAID' AND user_id='$user_id' ORDER BY id DESC") );
        $MIX_APP_File=env('MIX_APP_File');

        return ['orders'=>$orders,'MIX_APP_File'=>$MIX_APP_File];
    }
}
