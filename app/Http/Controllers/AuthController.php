<?php

namespace App\Http\Controllers;

use App\Models\OTP;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Knox\AFT\AFT;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'phone' => 'required',

        ]);
        if(empty($request->user_type)){
            return ['status'=>false,'message'=>'Please select account'];
        }
        if ($request->user_type=='teacher'){
            $request['status']='unapproved';
        }
        $credentials = request(['email', 'password']);
        $email=User::where('email',$request->email)->count();
        if($email>0){
            return ['status'=>false,'message'=>'Email is already in use'];
        }
        $request['password']=bcrypt($request->password);

        if ($request->user_type=='teacher'){
            if(!empty($request->file)) {
                if (($request->has('file'))) {
                        $file=$request->file;
                        $fileNameWithExt = $file->getClientOriginalName();
                        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                        $extesion = $file->getClientOriginalExtension();
                        $fileNameToStore = $filename . '_' . time() . '.' . $extesion;
                        $path = $file->storeAs('/public/Files', $fileNameToStore);
                        $request['file_url'] = $fileNameToStore;
                }else{
                    return ['status'=>false,'message'=>'An academic file must be uploaded']; 
                }
    
            }else{
                return ['status'=>false,'message'=>'An academic file must be uploaded'];
            }
        }

        $user=User::create($request->all());

        if(!Auth::attempt($credentials)) {
            return ['status' => false, 'message' => 'We are not able to log you in, please use login page'];
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);


        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];


    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid email or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);


        return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];
    }

    public  function user(){
        $user=User::find(Auth::user()->id);
        $wallet=Wallet::where('user_id',Auth::user()->id)->sum('amount');
        return ['user'=>$user,'wallet'=>$wallet];
    }


    public  function getCode(Request $request){
        $check=User::where('email',$request->email)->count();
        $user=User::where('email',$request->email)->first();
        if($check<=0){
            return ['status'=>false,'message'=>'The email address does not exist'];
        }
        $randomid = mt_rand(1000,9999);
        $request['password']=bcrypt($randomid);
        $update=User::where('id',$user->id)->update(['password'=>$request->password]);
       
        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }
        $message='Your One Time Password is '.$randomid.'.Please use this password to login.@EDUBORA';
        AFT::sendMessage($phone, $message);
        return ['status'=>true,'user'=>$user,'message'=>'A four digit OTP code has been sent,use the code as password to login'];
    }

    public function confirm(Request $request){
        $datas = DB::select( DB::raw("SELECT * FROM `o_t_p_s` WHERE phone='$request->phone' AND code='$request->code' AND created_at > NOW() - INTERVAL 4 HOUR") );
        if($datas==null){
            return ['status'=>false,'message'=>'Invalid code'];
        }
        return ['status'=>true,'message'=>'Success'];
    }

    public  function setPassword(Request $request){
        $user=User::find($request->id);
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='Yes';
        $user->update($request->all());
        return ['status'=>true,'message'=>'Password changed successfully, please login'];
    }

    public  function updateInfo(Request $request){
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Information updated successfully'];
    }
    public function updateUser(Request $request){
        $user=User::find($request->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Information updated successfully'];
    }

    public  function changePassword(Request $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }

        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is invalid'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='Yes';
        $user=User::find(Auth::user()->id);
        $user->update($request->all());

        return ['status'=>true,'message'=>'Password successfully set, you will now use your new password to login'];
    }
    public function users(){
        $users=User::where('user_type','admin')->get();
        return $users;
    }

    public function addUser(Request $request){
        $request->validate([
            'email' => 'required|string|email',
            'phone' => 'required',

        ]);
        $request['user_type']='admin';
        $request['role']='admin';
        $user=User::create($request->all());
        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }
        $message='You have been onboarded as admin on edubora .Please click forget passowrd to set new password.@EDUBORA';
        AFT::sendMessage($phone, $message);
        return ['status'=>true,'message'=>'User added ssuccesfully.'];
    }

    public function getUser($id){
        $user=User::find($id);
        return ['user'=>$user];
    }

}
