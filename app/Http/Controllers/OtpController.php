<?php

namespace App\Http\Controllers;

use App\Models\Tax;
use App\Models\User;
use App\Models\WithdrawalCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Knox\AFT\AFT;

class OtpController extends Controller
{
    public  function getWithdrawalCode(Request  $request){
        try {
            $id=Auth::user()->id;
            $code=WithdrawalCode::where('user_id',$id)->delete();
            $simu=User::find(Auth::user()->id);
            if(strlen($simu->phone)==10){
                $phone='254'.substr($simu->phone,1);
            }else{
                $phone=str_replace(' ','','254'.substr($simu->phone,4));
            }
            $deduction=Tax::sum('percent');
            $deductions=Tax::all();

            $request['code']=mt_rand(1000,9999);
            $request['user_id']=Auth::user()->id;
            $c=WithdrawalCode::create($request->all());
            $message='Your edubora withdrawal code is  '.$request->code;
            AFT::sendMessage($phone, $message);

            return ['status'=>true,'message'=>'Enter amount','phone'=>$phone,'deduction'=>$deduction,'user_id'=>$simu->id,'deductions'=>$deductions];
        } catch (\Exception $e) {
            return ['status'=>true,'data'=>$e->getMessage()];
        }
    }
}
