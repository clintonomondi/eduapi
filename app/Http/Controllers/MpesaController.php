<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\MpesaLogs;
use App\Models\Order;
use App\Models\OrderRecords;
use App\Models\Tax;
use App\Models\User;
use App\Models\Wallet;
use App\Models\WithdrawalCode;
use App\Models\WithdrawalLogs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Knox\AFT\AFT;
use MPESA;

class MpesaController extends Controller
{
public  function initiateSTK(Request $request,$id){
    try {
        $order=Order::find($id);
        if($order->status=='PAID'){
            return ['status'=>true,'ResponseMessage'=>'This order is already paid'];
        }
        $amount=OrderRecords::where('order_id',$id)->sum('amount');
        if(strlen($request->phone)==10){
            $phone='254'.substr($request->phone,1);
        }else{
            $phone=str_replace(' ','','254'.substr($request->phone,4));
        }
    $mpesa = MPESA::stkPush((int)$phone,(int)$amount,$order->order_no);

    $request['system_ref']='ED'.mt_rand(10000,99999).Auth::user()->id;
    $request['MerchantRequestID']=$mpesa->MerchantRequestID;
    $request['CheckoutRequestID']=$mpesa->CheckoutRequestID;
    $request['amount']=$amount;
    $request['phone']=$phone;
    $request['account']=$order->order_no;
    $request['ResponseCode']=$mpesa->ResponseCode;
    $request['ResponseDescription']=$mpesa->ResponseDescription;
    $request['order_id']=$id;
    $data=MpesaLogs::create($request->all());
        return ['status'=>true,'ResponseCode'=>$mpesa->ResponseCode,'ResponseMessage'=>$mpesa->CustomerMessage,
            'MerchantRequestID'=>$request->MerchantRequestID,'CheckoutRequestID'=>$request->CheckoutRequestID];

    } catch (\Exception $e) {
        return ['status'=>false,'ResponseMessage'=>$e->getMessage()];
    }
}
    


public  function callBack(Request $request){
    try {
        $response = $request->all();
//    Log::info($response['Body']['stkCallback']);
        $request['ResultCode'] = $response['Body']['stkCallback']['ResultCode'];
        $request['ResultDesc'] = $response['Body']['stkCallback']['ResultDesc'];
        $request['MerchantRequestID'] = $response['Body']['stkCallback']['MerchantRequestID'];
        $request['CheckoutRequestID'] = $response['Body']['stkCallback']['CheckoutRequestID'];
        if ($request->ResultCode == '0') {
            $collection = collect($response['Body']['stkCallback']['CallbackMetadata']['Item']);
            $transaction = $collection->where('Name', 'MpesaReceiptNumber')->first();
             $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
             $logs=MpesaLogs::find($info->id);
             if($logs->status=='PENDING'){
             $order=Order::find($logs->order_id);
             $request['status']='PAID';
             $request['mpesa_ref']=$transaction['Value'];
             $request['trans_id']=$transaction['Value'];
             $request['payment_ref']=$transaction['Value'];
             $request['system_id']=$logs->system_ref;
             $request['payment_method']='M-PESA EXPRESS';
             $logs->update($request->all());
             $order->update($request->all());
             $orderRe=OrderRecords::where('order_id',$order->id)->update(['status'=>'PAID','payment_ref'=>$request->payment_ref]);

            $recs=OrderRecords::where('order_id',$order->id)->get();
            foreach ($recs as $records){
                $book=Book::find($records['book_id']);
                $request['user_id']=$book->user_id;
                $balance=Wallet::where('user_id',$request->user_id)->sum('amount');
                $sumtax=Tax::sum('percent');
                $counttax=Tax::count();
                $p=$sumtax/$counttax;
                $percent=$records['amount']-($p/100 *$records['amount']);
                $amount_to_save=$balance+$percent;
                $wallet = DB::select( DB::raw("INSERT INTO wallets (amount,user_id)VALUES('$amount_to_save','$request->user_id') ON DUPLICATE KEY UPDATE amount='$amount_to_save'") );
            }


            $user=User::find($order->user_id);
            if(strlen($user->phone)==10){
                $phone=$user->phone;
            }else{
                $phone=str_replace(' ','','0'.substr($user->phone,4));
            }
            $message='Your payment for Order '.$order->order_no.' has been received.M-Pesa ref: '.$request->mpesa_ref.' .System ref: '.$request->system_id.'Thank you for using Edubora.';
            AFT::sendMessage($phone, $message);
        }
        }
        else{
            $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
            $logs=MpesaLogs::find($info->id);
            $request['status']='CANCELLED';
            $logs->update($request->all());
        }
    } catch (\Exception $e) {
        Log::info($e->getMessage());
    }
    return ['status'=>true];
}


public  function checkStkPayment(Request $request){
    $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
    if($info->status=='PAID'){
        return ['status'=>true,'info'=>$info];
    }else{
        return ['status'=>false,'info'=>$info];
    }

}
public  function mpesatrans(){
    $trans=DB::select( DB::raw("SELECT * ,
(SELECT order_no from orders B WHERE B.id=A.order_id)order_no
 FROM mpesa_logs A WHERE status='PAID' ORDER BY id DESC") );
    return ['trans'=>$trans];
}

public function withdrawals(){
    $trans=DB::select( DB::raw("SELECT * ,
(SELECT name from users B WHERE B.id=A.user_id)name
 FROM withdrawal_logs A WHERE status='COMPLETED' ORDER BY id DESC") );
    return ['trans'=>$trans]; 
}






public  function simulateC2B(Request $request){
//    $this->registerURL();
    $mpesa = MPESA::c2bSimulate(254712083128,2,'CustomerPayBillOnline','123456');
    return ['status'=>true,'mpesa'=>$mpesa];
}
public  function c2bConfirm(Request $request){
    try {
    $response = json_decode($request->getContent(), true);
    $order2=Order::where('order_no',$response['BillRefNumber'])->first();
    $order=Order::find($order2->id);
    $order_amount=MpesaLogs::where('order_id',$order->id)->sum('amount');


    $request['mpesa_ref']=$response['TransID'];
    $request['system_ref']='ED'.mt_rand(10000,99999);
    $request['MerchantRequestID']="NA";
    $request['CheckoutRequestID']="NA";
    $request['amount']=$response['TransAmount'];
    $request['account']=$response['BillRefNumber'];
    $request['ResponseCode']='NA';
    $request['ResponseDescription']='NA';
    $request['order_id']=$order->id;
    $request['phone']=$response['MSISDN'];
//    $request['OrgAccountBalance']=$response['OrgAccountBalance'];
    $amount_to_pay=$request->amount+$order_amount;

    if($amount_to_pay>=$order->amount){
        $request['status']='PAID';

        $request['payment_ref']=$request->mpesa_ref;
        $request['system_id']=$request->system_ref;
        $request['payment_method']='M-PESA PAYBILL';
        $order->update($request->all());
        $orderRe=OrderRecords::where('order_id',$order->id)->update(['status'=>'PAID','payment_ref'=>$request->payment_ref]);


    }else{
        $request['status']='PENDING';
    }
    $mpesalogs=MpesaLogs::create($request->all());

        $recs=OrderRecords::where('order_id',$order->id)->get();
        foreach ($recs as $records){
            $book=Book::find($records['book_id']);
            $request['user_id']=$book->user_id;
            $balance=Wallet::where('user_id',$request->user_id)->sum('amount');
            $sumtax=Tax::sum('percent');
            $counttax=Tax::count();
            $p=$sumtax/$counttax;
            $percent=$records['amount']-($p/100 *$records['amount']);
            $amount_to_save=$balance+$percent;
            $wallet = DB::select( DB::raw("INSERT INTO wallets (amount,user_id)VALUES('$amount_to_save','$request->user_id') ON DUPLICATE KEY UPDATE amount='$amount_to_save'") );
        }

        $user=User::find($order->user_id);
        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }
        $message='Your payment for Order '.$order->order_no.' has been received.M-Pesa ref: '.$request->mpesa_ref.' .System ref: '.$request->system_id.'Thank you for using Edubora.';
        AFT::sendMessage($phone, $message);

    } catch (\Exception $e) {
        Log::info($e->getMessage());
    }
    return ['status'=>true];
}



    public function registerURL(Request $request){
        $mpesa = MPESA::registerC2bUrl();
        Log::info(json_encode($mpesa));
        return ['mpesa'=>$mpesa];
    }

    public  function withdraw(Request $request){
        try {
        $simu=Auth::user()->phone;
        if(strlen($simu)==10){
            $phone='254'.substr($simu,1);
        }else{
            $phone=str_replace(' ','','254'.substr($simu,4));
        }

            $balance=Wallet::where('user_id',Auth::user()->id)->sum('amount');
            if($request->amount>$balance){
                return ['status'=>false,'message'=>'Your account has insufficient balance'];
            }
            $request['code']='EDCODE'.mt_rand(100000000,999999999).Auth::user()->id;
            $request['user_id']=Auth::user()->id;
            $c=WithdrawalCode::create($request->all());

        return ['status'=>true,'phone'=>$phone,'amount'=>$request->amount,'code'=>$request->code,'email'=>Auth::user()->email,'user_id'=>Auth::user()->id];
        } catch (\Exception $e) {
            return ['status'=>true,'data'=>$e->getMessage()];
        }
    }



    public  function b2cResult(Request $request){
        try {
            Log::info('Calling back B2C oooooooooooooh');
            Log::info($request);
            $request['ResultCode']=$request['Result']['ResultCode'];
            if ($request->ResultCode == '0') {
                $request['ConversationID']=$request['Result']['ConversationID'];
                $request['OriginatorConversationID']=$request['Result']['OriginatorConversationID'];
                $request['ResponseDescription']=$request['Result']['ResultDesc'];
                $request['TransactionID']=$request['Result']['TransactionID'];
//                $request['balance']=$request['Result']['ResultParameters']['ResultParameter'][4]['Value'];
                $request['date']=$request['Result']['ResultParameters']['ResultParameter'][3]['Value'];
                $request['user']=$request['Result']['ResultParameters']['ResultParameter'][2]['Value'];
                $request['account_balance']=$request['Result']['ResultParameters']['ResultParameter'][4]['Value'];
                $request['status']='COMPLETED';

                $d=WithdrawalLogs::where('ConversationID',$request->ConversationID)->where('OriginatorConversationID',$request->OriginatorConversationID)->first();
                $data=WithdrawalLogs::find($d->id);
                $data->update($request->all());
                $user=User::find($data->user_id);
                $wallet=Wallet::where('user_id',$user->id)->first();
                Log::info('Wallllllet->>>>>>>'.$wallet);

                if(strlen($user->phone)==10){
                    $phone=$user->phone;
                }else{
                    $phone=str_replace(' ','','0'.substr($user->phone,4));
                }
                $message='You have withdrawn Ksh.'.$data->amount.' from Edubora.M-Pesa Ref: '.$request->TransactionID.' . System Ref: '.$data->system_ref.' Balance Ksh.'.$wallet->amount;
                AFT::sendMessage($phone, $message);
            }

        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
        return ['status'=>true];
    }



}
