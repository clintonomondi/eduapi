<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Darasa;
use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{

    public  function post(Request $request,$id){
        $request['category_id']=$id;
        $data=Subject::create($request->all());
        return ['status'=>true,'message'=>'Data submitted successfully'];
    }

    public  function index($id){
        $category=Category::find($id);
        $data=Subject::where('category_id',$id)->get();
        return ['category'=>$category,'subjects'=>$data];
    }
    public  function getCategorySubjects(Request $request){
        $subjects=Subject::where('category_id',$request->category_id)->get();
        $darasa=Darasa::where('category_id',$request->category_id)->get();
        return ['subjects'=>$subjects,'darasa'=>$darasa];
    }

    public function updateSubject(Request $request){
        $su=Subject::find($request->id);
        $su->update($request->all());
        return ['status'=>true,'message'=>'Data updated successfully'];
    }
}
