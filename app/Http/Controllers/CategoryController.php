<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Darasa;
use App\Models\Subject;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
 public  function index(){
     $data=Category::all();
     $cat=array();

     foreach ($data as $category){
         $subjects=Subject::where('category_id',$category->id)->get();
         $darasa=Darasa::where('category_id',$category->id)->get();
         $count=Subject::where('category_id',$category->id)->count();
         $count2=Darasa::where('category_id',$category->id)->count();
         $cat[] = array(
             "id" => $category->id,
             "name" => $category->name,
             "subject_count" => $count,
             "darasa_count" => $count2,
             "created_at" => $category->created_at,
             "subjects" => $subjects,
             "darasa" => $darasa,
             "description" => $category->description,
         );
     }

     return ['category'=>$cat];
 }

 public  function post(Request $request){
     $data=Category::create($request->all());
     return ['status'=>true,'message'=>'Data submitted successfully'];
 }

 public  function getCategory($id){
     $category=Category::find($id);
     return ['category'=>$category];
 }

 public  function updateCategory(Request $request,$id){
     $cat=Category::find($id);
     $cat->update($request->all());
     return ['status'=>true,'message'=>'Data updated successfully'];
 }
}
