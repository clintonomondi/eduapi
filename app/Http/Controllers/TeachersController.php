<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Knox\AFT\AFT;

class TeachersController extends Controller
{
    public  function index(){
        $data = DB::select( DB::raw("SELECT *,
        (SELECT SUM(amount) from wallets B WHERE B.user_id=A.id)amount
         from users A WHERE  user_type='teacher' ORDER BY id DESC") );
        return ['teachers'=>$data];
    }

    public  function approve(Request $request){
        $data=User::find($request->id);
        if($request->status==='Active'){
            $data->update($request->all());
            $message='Hi,'.$data->name.',your application as a tutor was Approved.@Edubora';
        }else{
            $data->delete();
            $message='Hi,your application as a tutor was Rejected.Please contact admin.@Edubora';
        }
        if(strlen($data->phone)==10){
            $phone=$data->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($data->phone,4));
        }
        AFT::sendMessage($phone, $message);
        return ['status'=>true,'message'=>'Information updated successfully'];
    }
}
