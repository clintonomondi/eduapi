<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use App\Models\Darasa;
use App\Models\File;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BooksController extends Controller
{
    public  function post(Request $request){
        $validatedData = $request->validate([
            'title'=>'required',
        ]);
        if(Auth::user()->user_type!='teacher'){
            return ['status'=>false,'message'=>'You are not authorised!'];   
        }
        $request['user_id']=Auth::user()->id;
        $request['url']='NA';
        if(!empty($request->files)) {
            
            if (($request->has('files'))) {
                $data=Book::create($request->all());
                $request['book_id']=$data->id;

                $files = $request->file('files');
                foreach ($files as $file) {
                    $fileNameWithExt = $file->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extesion = $file->getClientOriginalExtension();
                    $fileNameToStore = $filename . '_' . time() . '.' . $extesion;
                    $path = $file->storeAs('/public/Books', $fileNameToStore);
                    $request['url'] = $fileNameToStore;
                    $doc = File::create($request->all());
                }
            }else{
                return ['status'=>false,'message'=>'You must upload atleast one file'];  
            }

        }else{
            return ['status'=>false,'message'=>'You must upload atleast one file'];
        }


        return ['status'=>true,'message'=>'Item uploaded successfully'];
    }
    public  function edit(Request $request,$id){
        $validatedData = $request->validate([
            'title'=>'required',
        ]);
        if(Auth::user()->user_type!='teacher'){
            return ['status'=>false,'message'=>'You are not authorised!'];   
        }
        $book=Book::find($id);
        $request['user_id']=Auth::user()->id;

        if(!empty($request->files)) {
            $request['book_id']=$id;
            if (($request->has('files'))) {
                $files = $request->file('files');
                foreach ($files as $file) {
                    $fileNameWithExt = $file->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extesion = $file->getClientOriginalExtension();
                    $fileNameToStore = $filename . '_' . time() . '.' . $extesion;
                    $path = $file->storeAs('/public/Books', $fileNameToStore);
                    $request['url'] = $fileNameToStore;
                    $doc = File::create($request->all());
                }
            }

        }

        $data=$book->update($request->all());
        return ['status'=>true,'message'=>'Item updated successfully'];
    }

    public  function removeFile(Request $request,$id){
        if(Auth::user()->user_type!='teacher'){
            return ['status'=>false,'message'=>'You are not authorised!'];   
        }
        $file=File::find($request->file_id);
        Storage::delete('/public/Books/'.$request->url);
        $file->delete();
        return ['status'=>true,'message'=>'File removed successfully'];
    }

    public  function getFiles(Request $request){
        $files=File::where('book_id',$request->book_id)->get();
        return ['files'=>$files];
    }

    public  function myitems(){
        if(Auth::user()->user_type!='teacher'){
            return ['status'=>false,'message'=>'Yu are not authorised!'];   
        }
      $user_id=Auth::user()->id;
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A WHERE user_id='$user_id'") );
        $MIX_APP_File=env('MIX_APP_File');
        return ['books'=>$books,'MIX_APP_File'=>$MIX_APP_File];
    }

    public  function getBooksBySubject($id){
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A WHERE subject_id='$id'") );
//        $level=DB::select( DB::raw("SELECT level FROM books GROUP BY level") );

        return ['books'=>$books];
    }

    public  function getBooksByClasses($id){
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A WHERE darasa_id='$id'") );
//        $level=DB::select( DB::raw("SELECT level FROM books GROUP BY level") );
        $cat=Category::all();
        return ['books'=>$books,'category'=>$cat];
    }

    public  function getBooksByCategory($id){
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A WHERE category_id='$id'") );
//        $level=DB::select( DB::raw("SELECT level FROM books GROUP BY level") );
        $subjects=Subject::where('category_id',$id)->get();
        $level=Darasa::where('category_id',$id)->get();
        $cat=Category::all();
        return ['books'=>$books,'subjects'=>$subjects,'level'=>$level,'category'=>$cat];
    }

    public  function getBooksById($id){
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A WHERE id='$id'") );
        $files=File::where('book_id',$id)->get();

        return ['books'=>$books,'files'=>$files];
    }


    public  function getBooksByIdCheckout($id){
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A WHERE id='$id'") );

        return ['books'=>$books];
    }

    public  function getBooksAll(){
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A ") );
//        $level=DB::select( DB::raw("SELECT level FROM books GROUP BY level") );
        $cat=Category::all();
        return ['books'=>$books,'category'=>$cat];
    }

    public  function getBooksByrandSearch(Request $request){
        $books=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM categories B WHERE B.id=A.category_id)category,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT NAME FROM darasas B WHERE B.id=A.darasa_id)darasa
 FROM books A WHERE title LIKE '$request->keyword%' OR LEVEL LIKE '$request->keyword%'") );
        $level=DB::select( DB::raw("SELECT level FROM books GROUP BY level") );
        $cat=Category::all();
        return ['books'=>$books,'category'=>$cat];
    }

    public  function getBooksByLevel(Request $request){
        $books=Book::where('darasa_id',$request->darasa_id)->get();
        return ['books'=>$books];
    }

}
