<?php

namespace App\Http\Controllers;

use App\Models\Tax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use MPESA;

class MoneyController extends Controller
{

public  function taxes(){
    $taxes=Tax::all();
    return ['taxes'=>$taxes];
}

public  function addtaxes(Request $request){

    $request->validate([
        'percent' => 'required|numeric',
    ]);
    $data=Tax::create($request->all());
    return ['status'=>true,'message'=>'Tax added successfully'];
}

public  function updatetaxes(Request $request){
    $request->validate([
        'percent' => 'required|numeric',
    ]);
    $data=Tax::find($request->id);
    $data->update($request->all());
    return ['status'=>true,'message'=>'Tax updated successfully'];
}
}
