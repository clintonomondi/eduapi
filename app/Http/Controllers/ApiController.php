<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\File;
use App\Models\MpesaLogs;
use App\Models\Order;
use App\Models\OrderRecords;
use App\Models\User;
use App\Models\WithdrawalLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public  function dashboard(){
        $teachers=User::where('user_type','teacher')->count();
        $client=User::where('user_type','client')->count();
        return['teachers'=>$teachers,'clients'=>$client];

    }

    public  function getClientIndexPage(){
        $books=Book::orderBy('id','desc')->first();
        return ['books'=>$books];
    }

    public function getAdminDashboard(){
        $items=Book::count();
        $teachers=User::where('user_type','teacher')->count();
        $clients=User::where('user_type','client')->count();
        $withdwals=WithdrawalLogs::where('status','COMPLETED')->sum('amount');
        $payments=MpesaLogs::where('status','PAID')->sum('amount');
        $files=File::count();


        $year=date("Y");
        $data=DB::select( DB::raw("SELECT
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='1'  AND YEAR(updated_at)='$year')jan,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='2'  AND YEAR(updated_at)='$year')feb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='3'  AND YEAR(updated_at)='$year')mar,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='4'  AND YEAR(updated_at)='$year')apr,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='5' AND YEAR(updated_at)='$year')may,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='6'  AND YEAR(updated_at)='$year')jun,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='7'  AND YEAR(updated_at)='$year')jul,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='8'  AND YEAR(updated_at)='$year')aug,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='9'  AND YEAR(updated_at)='$year')sep,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='10'  AND YEAR(updated_at)='$year')octb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='11'  AND YEAR(updated_at)='$year')nov,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='12'  AND YEAR(updated_at)='$year')dece
 FROM DUAL"));

        return ['items'=>$items,'teachers'=>$teachers,'clients'=>$clients,'withdrawals'=>$withdwals,'payments'=>$payments,'files'=>$files,'info'=>$data];
    }

    public function getClientDashboard(){
        $items=Book::where('user_id',Auth::user()->id)->count();
        $orders=Order::where('user_id',Auth::user()->id)->count();
        $downloads=OrderRecords::where('user_id',Auth::user()->id)->where('status','PAID')->count();
        $transactions=WithdrawalLogs::where('user_id',Auth::user()->id)->count();
        return ['items'=>$items,'orders'=>$orders,'downloads'=>$downloads,'transactions'=>$transactions];
    }

    public function getLogsClient(){
        $logs=WithdrawalLogs::orderBy('id','desc')->where('user_id',Auth::user()->id)->get();
        return ['logs'=>$logs];
    }

    public function getClients(){
        $data = DB::select( DB::raw("SELECT *,
        (SELECT SUM(amount) from wallets B WHERE B.user_id=A.id)amount
         from users A WHERE  user_type='client' ORDER BY id DESC") );
        return ['teachers'=>$data];
    }
}
