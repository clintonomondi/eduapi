<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawalLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_logs', function (Blueprint $table) {
            $table->id();
            $table->string('amount');
            $table->string('amount_to_pay');
            $table->string('charges');
            $table->string('TransactionID')->nullable();
            $table->string('balance')->nullable();
            $table->string('phone')->nullable();
            $table->string('system_ref')->nullable();
            $table->string('ConversationID')->nullable();
            $table->string('OriginatorConversationID')->nullable();
            $table->string('ResponseCode')->nullable();
            $table->text('ResponseDescription')->nullable();
            $table->string('status')->default('PENDING');
            $table->string('date')->nullable();
            $table->string('user')->nullable();
            $table->string('user_id');
            $table->string('amount_received')->nullable();
            $table->string('withdraw_charges')->nullable();
            $table->string('account_balance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_logs');
    }
}
