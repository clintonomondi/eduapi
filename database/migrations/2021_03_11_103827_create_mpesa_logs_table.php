<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMpesaLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpesa_logs', function (Blueprint $table) {
            $table->id();
            $table->string('mpesa_ref')->nullable();
            $table->string('system_ref');
            $table->string('MerchantRequestID');
            $table->string('CheckoutRequestID');
            $table->string('amount');
            $table->string('account');
            $table->string('ResponseCode')->nullable();
            $table->text('ResponseDescription')->nullable();
            $table->string('status')->default('PENDING');
            $table->string('order_id');
            $table->string('phone');
            $table->string('ResultCode')->nullable();
            $table->string('ResultDesc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpesa_logs');
    }
}
