<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,600) as $index) {
            DB::table('books')->insert([
                'user_id' => 1,
                'category_id' => $faker->numberBetween(1,4),
                'darasa_id' => $faker->numberBetween(1,30),
                'subject_id' => $faker->numberBetween(1,40),
                'title' => $faker->paragraph(1),
                'description' => $faker->paragraph(2),
                'url' => 'Item_1615269131.pdf',
                'amount' => $faker->numberBetween(100,1000),
                'level' => $faker->randomElement($array = array ('Paper 1','Paper 2','paper 3','Revision exams','Term 4 exams','Education project')),
            ]);
        }
    }
}
