<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'System Admin',
            'email' => 'admin@gmail.com',
            'role' => 'admin',
            'user_type' => 'admin',
            'phone' => '0712083128',
            'status' => 'Active',
            'password' => bcrypt('admin'),
        ]);
    }
}
