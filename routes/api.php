<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::post('getCode', 'App\Http\Controllers\AuthController@getCode');
    Route::post('confirm', 'App\Http\Controllers\AuthController@confirm');
    Route::post('setPassword', 'App\Http\Controllers\AuthController@setPassword');

    Route::get('category', 'App\Http\Controllers\CategoryController@index');
    Route::get('subject/{id}', 'App\Http\Controllers\SubjectController@index');
    Route::post('updateSubject', 'App\Http\Controllers\SubjectController@updateSubject');
    Route::get('darasa/{id}', 'App\Http\Controllers\DarasaController@index');
    Route::get('getClientIndexPage', 'App\Http\Controllers\ApiController@getClientIndexPage');
    Route::get('getBooksBySubject/{id}', 'App\Http\Controllers\BooksController@getBooksBySubject');
    Route::get('getBooksByClasses/{id}', 'App\Http\Controllers\BooksController@getBooksByClasses');
    Route::get('getBooksByCategory/{id}', 'App\Http\Controllers\BooksController@getBooksByCategory');
    Route::get('getBooksById/{id}', 'App\Http\Controllers\BooksController@getBooksById');
    Route::get('getBooksAll', 'App\Http\Controllers\BooksController@getBooksAll');
    Route::post('getBooksByrandSearch', 'App\Http\Controllers\BooksController@getBooksByrandSearch');
    Route::post('getBooksByLevel', 'App\Http\Controllers\BooksController@getBooksByLevel');

    Route::post('callBack', 'App\Http\Controllers\MpesaController@callBack');
    Route::post('c2bConfirm', 'App\Http\Controllers\MpesaController@c2bConfirm');
    Route::post('registerURL', 'App\Http\Controllers\MpesaController@registerURL');
    Route::post('b2cResult', 'App\Http\Controllers\MpesaController@b2cResult');
    
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        //Clients
        Route::get('getClients', 'App\Http\Controllers\ApiController@getClients');
        //category
        Route::post('category', 'App\Http\Controllers\CategoryController@post');
        Route::get('category/{id}', 'App\Http\Controllers\CategoryController@getCategory');
        Route::post('category/update/{id}', 'App\Http\Controllers\CategoryController@updateCategory');

        //Subject
        Route::post('subject/{id}', 'App\Http\Controllers\SubjectController@post');
        Route::post('getCategorySubjects', 'App\Http\Controllers\SubjectController@getCategorySubjects');

        //Darasa
        Route::post('adddarasa', 'App\Http\Controllers\DarasaController@post');
        Route::post('getCategoryDarasa', 'App\Http\Controllers\DarasaController@getCategoryDarasa');
        Route::post('updateDarasa', 'App\Http\Controllers\DarasaController@updateDarasa');

        //Dashbord
        Route::get('dashboard', 'App\Http\Controllers\ApiController@dashboard');

        //teachers
        Route::get('teachers', 'App\Http\Controllers\TeachersController@index');
        Route::post('approve', 'App\Http\Controllers\TeachersController@approve');
        Route::get('users', 'App\Http\Controllers\AuthController@users');
        Route::post('addUser', 'App\Http\Controllers\AuthController@addUser');
        Route::get('getUser/{id}', 'App\Http\Controllers\AuthController@getUser');
        Route::post('updateUser', 'App\Http\Controllers\AuthController@updateUser');


        //books
        Route::post('books', 'App\Http\Controllers\BooksController@post');
        Route::post('books/edit/{id}', 'App\Http\Controllers\BooksController@edit');
        Route::post('removeFile/{id}', 'App\Http\Controllers\BooksController@removeFile');
        Route::post('getFiles', 'App\Http\Controllers\BooksController@getFiles');
        Route::get('books', 'App\Http\Controllers\BooksController@myitems');
        Route::get('getBooksByIdCheckout/{id}', 'App\Http\Controllers\BooksController@getBooksByIdCheckout');


        //Dashboard
        Route::get('getClientDashboard', 'App\Http\Controllers\ApiController@getClientDashboard');
        Route::get('getAdminDashboard', 'App\Http\Controllers\ApiController@getAdminDashboard');
        Route::get('getLogsClient', 'App\Http\Controllers\ApiController@getLogsClient');
        //Orders
        Route::post('placeOrder', 'App\Http\Controllers\OrderController@placeOrder');
        Route::get('getOrder/{id}', 'App\Http\Controllers\OrderController@getOrder');
        Route::get('orders', 'App\Http\Controllers\OrderController@orders');
        Route::get('user', 'App\Http\Controllers\AuthController@user');
        Route::get('downloads', 'App\Http\Controllers\OrderController@downloads');

        //Mpesa
        Route::post('initiateSTK/{id}', 'App\Http\Controllers\MpesaController@initiateSTK');
        Route::post('checkStkPayment', 'App\Http\Controllers\MpesaController@checkStkPayment');
        Route::post('simulateC2B', 'App\Http\Controllers\MpesaController@simulateC2B');
        Route::get('transactions/mpesa', 'App\Http\Controllers\MpesaController@mpesatrans');
        Route::get('transactions/withdrawals', 'App\Http\Controllers\MpesaController@withdrawals');

        Route::post('withdraw', 'App\Http\Controllers\MpesaController@withdraw');

        //User
        Route::post('updateInfo', 'App\Http\Controllers\AuthController@updateInfo');
        Route::post('changePassword', 'App\Http\Controllers\AuthController@changePassword');

        //Post
        Route::post('post', 'App\Http\Controllers\PostController@post');
        Route::get('getPosts', 'App\Http\Controllers\PostController@getPosts');

        //Tax
        Route::get('taxes', 'App\Http\Controllers\MoneyController@taxes');
        Route::post('taxes', 'App\Http\Controllers\MoneyController@addtaxes');
        Route::post('taxes/update', 'App\Http\Controllers\MoneyController@updatetaxes');

        Route::post('getWithdrawalCode', 'App\Http\Controllers\OtpController@getWithdrawalCode');

    });
});
